## Preconfig
```
export KUBECONFIG=your-cluster-kubeconfig.yaml
```
## Start project
```
kubectl apply -f ./operator/

```
## also you can see lots of k8s-abstractions in ./kubernetes in order to apply it use
```
kubectl apply -f ./kubernetes/
```
